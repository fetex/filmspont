import json
import datetime
import http.client
from time import time

########################################################################################################################
##################################################### ENVIRONMENTS #####################################################
########################################################################################################################

#local
#conn = http.client.HTTPConnection("localhost:5008")

#container
conn = http.client.HTTPConnection("localhost:5000")

########################################################################################################################
######################################################## ROLES  #########################################################
########################################################################################################################

#headers = {
#    'Content-type': 'application/json',
#    'authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtYyIsImlkZW50aXR5IjoibWMiLCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwiaWF0IjoxNTY5OTY3NTc5LCJleHAiOjE1Njk5NjgxNzl9.lENZk0cCxr6XkV21071970VYj9oVgtwc3RBj2P26-RJHAT6FiUSzELE8ntNOgq-0OHvQ2PhCSskEkZo3dtVg0A'
#}
#conn.request("GET", "/rol?page=2", headers=headers)


#conn.request("GET", "/rol?page=0&name=%director%", headers={'Content-type': 'application/json'})


#conn.request("GET", "/rol/1", headers={'Content-type': 'application/json'})

#create_rol_post = {
#     'name': 'coWriter32'
# }
#json_data_post = json.dumps(create_rol_post)
#conn.request("POST", "/rol", json_data_post, headers={'Content-type': 'application/json'})

#create_rol_post = {
#     'name': 'coWriter8' 
#}
#json_data_post = json.dumps(create_rol_post)
#conn.request("PUT", "/rol/12", json_data_post, headers={'Content-type': 'application/json'})

#conn.request("DELETE", "/rol/9", headers={'Content-type': 'application/json'})

########################################################################################################################
####################################################### MOVIES #########################################################
########################################################################################################################


#create_movie_post = {
#     'name': 'Harry Potter2',
#     'des': 'Niño no busca piedra',
#     'year': '2002'
# }
#json_data_post = json.dumps(create_movie_post)
#conn.request("POST", "/movie", json_data_post, headers={'Content-type': 'application/json'})

#conn.request("GET", "/movie/1", headers={'Content-type': 'application/json'})

########################################################################################################################
###################################################### PERSONS #########################################################
########################################################################################################################

#conn.request("GET", "/person/1", headers={'Content-type': 'application/json'})



########################################################################################################################
################################################# METODOS ENTREGA FINAL ################################################
########################################################################################################################

#####Metodos roles

### /rol/get-all
#headers = {
#    'Content-type': 'application/json',
#    'authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtYyIsImlkZW50aXR5IjoibWMiLCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwiaWF0IjoxNTY5OTY3NTc5LCJleHAiOjE1Njk5NjgxNzl9.lENZk0cCxr6XkV21071970VYj9oVgtwc3RBj2P26-RJHAT6FiUSzELE8ntNOgq-0OHvQ2PhCSskEkZo3dtVg0A'
#}
#conn.request("GET", "/rol?page=2", headers=headers)

### /rol/get-specific
#conn.request("GET", "/rol/13", headers={'Content-type': 'application/json'})

### /rol/post
#create_rol_post = {
#     'name': 'Designer'
# }
#json_data_post = json.dumps(create_rol_post)
#conn.request("POST", "/rol", json_data_post, headers={'Content-type': 'application/json'})

####Metodos movies

## /movies/ get-all
conn.request("GET", "/movie?page=0", headers={'Content-type': 'application/json'})

## /movies/ post
#create_movie_post = {
#     'name': 'coWriter32'
# }
#json_data_post = json.dumps(create_rol_post)
#conn.request("POST", "/rol", json_data_post, headers={'Content-type': 'application/json'})

#/movie/post
#create_movie_post = {
#	   'name': 'Batman3',
#       'description': 'Batman3',
#       'stars' : 10,
#       'year': 1990
#}
#json_data_post = json.dumps(create_movie_post)
#conn.request("POST", "/movie", json_data_post, headers={'Content-type': 'application/json'})

## /movies/put
#edit_movie_put = {
#     'name': 'Batman2',
#     'description': 'Batman2',
#     'stars' : 7,
#     'year': 1990
#}
#json_data_post = json.dumps(edit_movie_put)
#conn.request("PUT", "/movie/3", json_data_post, headers={'Content-type': 'application/json'})
##Verificar
#conn.request("GET", "/movie/3", headers={'Content-type': 'application/json'})

start = datetime.datetime.now()
res = conn.getresponse()
end = datetime.datetime.now()
	
data = res.read()

elapsed = end - start

print(data.decode("utf-8"))
print("\"" + str(res.status) + "\"")
print("\"" + str(res.reason) + "\"")
print("\"elapsed seconds: " + str(elapsed) + "\"")

